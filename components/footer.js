const footerTemplate = document.createElement('template');

footerTemplate.innerHTML = `
	<style>
		@import url("../styles/bootstrap.min.css");
		@import url("../styles/index.css");
		@import url("https://cdn.jsdelivr.net/npm/fork-awesome@1.2.0/css/fork-awesome.min.css");
	</style>
	
    <footer class="bg-light text-center text-lg-start mt-6">
        <!-- Grid container -->
        <div class="container p-2">
            <div class="row footer__container">
                <div class="col-lg-6 col-md-12 mb-4 mb-md-0">
                    <h5 class="text-uppercase text-center">Atendimentos</h5>
                    <p class="text-center">
                        Praia do Rosa - Santa Catarina - Brasil
                        <br>
                        Garopaba - Santa Catarina - Brasil
                        <br>
                        Dois Irmãos - Rio Grande do Sul - Brasil
                    </p>
                </div>
                <!--Grid column-->

                <!--Grid column-->
                <div class="col-lg-6 col-md-12 mb-4 mb-md-0">
                    <h5 class="text-uppercase text-center">Contatos</h5>
                    <p class="text-center">
                        <a href="https://wa.me/5551996848165">+55 51 9684-8165</a>
                        <br>
                        <a href="https://instagram.com/daiane.ordemdohumano">@daiane.ordemdohumano</a>
                        <br>
                        <a href="mailto:sdaiane1989@gmail.com?subject=Contato pelo site&body=Sua mensagem">sdaiane1989@gmail.com</a>
                    </p>
                </div>
            </div>
        </div>
        <!-- Grid container -->

        <!-- Copyright -->
        <div class="text-center p-2 footer__dev" style="background-color: rgba(0, 0, 0, 0.1);">
            2022. Developed by <a target="_blank" class="text-dark" href="https://instagram.com/oidiossincratico">Daniel Pedrotti</a>
            <i class="fa fa-gitlab" aria-hidden="true"></i> <a target="_blank" class="text-dark" href="https://gitlab.com/w1446/ordem-do-humano"> gitlab </a>
        </div>
        <!-- Copyright -->
    </footer>
`;

class Footer extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    const fontAwesome = document.querySelector('link[href*="font-awesome"]');
    const shadowRoot = this.attachShadow({ mode: 'closed' });

    if (fontAwesome) {
      shadowRoot.appendChild(fontAwesome.cloneNode());
    }

    shadowRoot.appendChild(footerTemplate.content);
  }
}

customElements.define('footer-component', Footer);